package com.qiu.firechill.common.retrun;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @Authhor: qiu
 * @Date: 2020/10/25 10:41 上午
 */
public class ReturnDeal {

    /**
     * 往return中注入各种类型的值
     * @param lable sql中的字段名
     * @param obj   空的返回值
     * @param method  拼装起来的返回值中的set方法
     * @param rs   返回值注入器
     */
    public static void valWiredToReturn(String lable, Object obj, Method method, ResultSet rs) {
        try {
            if(rs.getObject(lable) instanceof String ){
                method.invoke(obj,rs.getString(lable));
            }
            else if(rs.getObject(lable) instanceof Integer){
                method.invoke(obj,rs.getInt(lable));
            }
            else if(rs.getObject(lable) == null){
                //System.out.println("is null"); do nothing
            }
            else{
                method.invoke(obj,rs.getObject(lable));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
